using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class SnapperWindow : EditorWindow
{
    const string UNDO_STR_SNAP = "snap objects";

    [MenuItem("Tools/Snapper")]
    public static void OpenSnapperWindow() => GetWindow<SnapperWindow>("Snapper");

    private void OnEnable()
    {
        Selection.selectionChanged += Repaint;
    }

    private void OnDisable()
    {
        Selection.selectionChanged -= Repaint;
    }

    private void OnGUI()
    {

        GUILayout.Label("Snap Degree:");
        string snapValueString = EditorGUILayout.TextField(Snapper.degree.ToString());


        // Try parsing the string to int
        if (int.TryParse(snapValueString, out int result))
        {
            Snapper.degree = result;
        }

        using (new EditorGUI.DisabledScope(Selection.gameObjects.Length == 0))
        {
            if (GUILayout.Button("Snap Selection"))
            {
                SnapSelection();
            }
        }


        GUILayout.Label("Google Snapping Sphere Radius:");
        string sphereRadiusString = EditorGUILayout.TextField(Snapper.sphereRadius.ToString());


        // Try parsing the string to int
        if (float.TryParse(sphereRadiusString, out float resultFloat))
        {
            Snapper.sphereRadius = resultFloat;
        }

        GUILayout.Label("Snapping Distance:");
        string snappingDistanceString = EditorGUILayout.TextField(Snapper.minSnappingDistance.ToString());


        // Try parsing the string to int
        if (float.TryParse(snappingDistanceString, out resultFloat))
        {
            Snapper.minSnappingDistance = resultFloat;
        }



    }

    void SnapSelection()
    {
        foreach (GameObject go in Selection.gameObjects)
        {
            Undo.RecordObject(go.transform, UNDO_STR_SNAP);
            go.transform.position = go.transform.position.Round();
        }
    }

}
