using System;
using UnityEngine;
using UnityEditor;
using UnityEngine.UIElements;


public static class Snapper
{
    public static int degree = 0;


    private static GameObject initialGO;
    private static Vector3 pastPosition;
    private static Quaternion pastRotation;
    private static Collider selectedCollider;

    private enum axisMove { x, y, z }
    private static axisMove axisMoving;

    public static float sphereRadius = 5;

    public static float minSnappingDistance = .2f;
    public static float minSnappingAngle = 10f;

    public static float minSnappingMoveAway;

    const string UNDO_STR_SNAP = "snap objects";

    [MenuItem("Edit/Snap Selected Objects %&S", isValidateFunction: true)]
    public static bool SnapTheThingValidate()
    {
        return Selection.gameObjects.Length > 0;
    }

    [MenuItem("Edit/Snap Selected Objects %&S")]
    public static void SnapTheThing()
    {


        foreach (GameObject go in Selection.gameObjects)
        {
            Undo.RecordObject(go.transform, UNDO_STR_SNAP);
            go.transform.position = go.transform.position.Round();

        }
    }


    [MenuItem("Edit/GoogleSlidesSnap &#S", isValidateFunction:true)]
    public static bool GoogleSlidesValidate()
    {
        return Selection.gameObjects.Length == 1;
    }


    [MenuItem("Edit/GoogleSlidesSnap &#S")]
    public static void GoogleSlides()
    {
        minSnappingMoveAway = minSnappingDistance * 2;
        Tool currTool = Tools.current;

        

        if(currTool == Tool.Move)
        {
            GameObject selectedGO = Selection.gameObjects[0];
            Transform selectedTran = selectedGO.transform;


            Undo.RecordObject(selectedGO.transform, UNDO_STR_SNAP);


            if ((initialGO == null) || (initialGO && initialGO != selectedGO))
            {
                initialGO = selectedGO;
                pastPosition = selectedGO.transform.position;
                selectedCollider = selectedGO.GetComponent<Collider>();
                return;
            }

            
            

            if (pastPosition.x != selectedGO.transform.position.x)
                axisMoving = axisMove.x;

            else if (pastPosition.y != selectedGO.transform.position.y)
                axisMoving = axisMove.y;

            else
                axisMoving = axisMove.z;



            


            Collider[] colliders = Physics.OverlapSphere(selectedTran.position, sphereRadius);

            foreach (Collider collider in colliders)
            {
                if (collider.gameObject == selectedGO)
                {
                    continue;
                }

                Vector3 colliderPos = collider.gameObject.transform.position;
                if (axisMoving == axisMove.x)
                {
                    float distance = Mathf.Abs(colliderPos.x - selectedTran.position.x);
                    if (distance < minSnappingDistance)
                    {
                        selectedTran.position = new Vector3(colliderPos.x, selectedTran.position.y, selectedTran.position.z);
                        Debug.DrawLine(selectedTran.position, colliderPos, Color.red, 1);
                        return;
                    }
                    

                }
                else if (axisMoving == axisMove.y)
                {
                    float distance = Mathf.Abs(colliderPos.y - selectedTran.position.y);
                    if (distance < minSnappingDistance)
                    {
                        selectedTran.position = new Vector3(selectedTran.position.x, colliderPos.y, selectedTran.position.z);
                        Debug.DrawLine(selectedTran.position, colliderPos, Color.red, 1);
                        return;
                    }
                }
                else
                {
                    float distance = Mathf.Abs(colliderPos.z - selectedTran.position.z);

                    if (distance < minSnappingDistance)
                    {
        
                        selectedTran.position = new Vector3(selectedTran.position.x, selectedTran.position.y, colliderPos.z);
                        Debug.DrawLine(selectedTran.position, colliderPos, Color.red, 1);
                        return;
                    }
                }

                
            }

            if (axisMoving == axisMove.x)
            {
                float selectedBoundExtentX = selectedCollider.bounds.extents.x;

                colliders = Physics.OverlapSphere(selectedTran.position + new Vector3(selectedBoundExtentX,0,0), sphereRadius);
                foreach (Collider collider in colliders)
                {
                    if (collider.gameObject == selectedGO)
                    {
                        continue;
                    }

                    Vector3 colliderPos = collider.gameObject.transform.position;
                    float selectedXMax = selectedCollider.bounds.max.x;


                    float compareXMax = collider.bounds.max.x;
                    float compareXMin = collider.bounds.min.x;

                    float distance = compareXMax - selectedXMax;
                    if (Mathf.Abs(distance) < minSnappingDistance)
                    {
                        selectedTran.position = new Vector3(selectedTran.position.x + distance, selectedTran.position.y, selectedTran.position.z);
                        Debug.DrawLine(selectedTran.position + new Vector3(selectedCollider.bounds.extents.x, 0, 0), colliderPos + new Vector3(collider.bounds.extents.x, 0, 0), Color.red, 1);
                        return;
                    }

                    distance = compareXMin - selectedXMax;
                    if (Mathf.Abs(distance) < minSnappingDistance)
                    {
                        selectedTran.position = new Vector3(selectedTran.position.x + distance, selectedTran.position.y, selectedTran.position.z);
                        Debug.DrawLine(selectedTran.position + new Vector3(selectedCollider.bounds.extents.x, 0, 0), colliderPos - new Vector3(collider.bounds.extents.x, 0, 0), Color.red, 1);
                        return;
                    }

                }

                colliders = Physics.OverlapSphere(selectedTran.position - new Vector3(selectedBoundExtentX, 0, 0), sphereRadius);
                foreach (Collider collider in colliders)
                {
                    if (collider.gameObject == selectedGO)
                    {
                        continue;
                    }

                    Vector3 colliderPos = collider.gameObject.transform.position;
                    float selectedXMin = selectedCollider.bounds.min.x;

                    float compareXMax = collider.bounds.max.x;
                    float compareXMin = collider.bounds.min.x;

                    
                    float distance = compareXMin - selectedXMin;
                    if (Mathf.Abs(distance) < minSnappingDistance)
                    {
                        selectedTran.position = new Vector3(selectedTran.position.x + distance, selectedTran.position.y, selectedTran.position.z);
                        Debug.DrawLine(selectedTran.position - new Vector3(selectedCollider.bounds.extents.x, 0, 0), colliderPos - new Vector3(collider.bounds.extents.x, 0, 0), Color.red, 1);
                        return;
                    }

                    distance = compareXMax - selectedXMin;
                    if (Mathf.Abs(distance) < minSnappingDistance)
                    {
                        selectedTran.position = new Vector3(selectedTran.position.x + distance, selectedTran.position.y, selectedTran.position.z);
                        Debug.DrawLine(selectedTran.position - new Vector3(selectedCollider.bounds.extents.x, 0, 0), colliderPos + new Vector3(collider.bounds.extents.x, 0, 0), Color.red, 1);
                        return;
                    }

                }


            }
            else if( axisMoving == axisMove.y)
            {
                float selectedBoundExtentY = selectedCollider.bounds.extents.y;

                float selectedYMax = selectedCollider.bounds.max.y;
                float selectedYMin = selectedCollider.bounds.min.y;

                colliders = Physics.OverlapSphere(selectedTran.position + new Vector3(0, selectedBoundExtentY, 0), sphereRadius);
                foreach (Collider collider in colliders)
                {
                    if (collider.gameObject == selectedGO)
                    {
                        continue;
                    }

                    Vector3 colliderPos = collider.gameObject.transform.position;
                    


                    float compareYMax = collider.bounds.max.y;
                    float compareYMin = collider.bounds.min.y;

                    float distance = compareYMax - selectedYMax;
                    if (Mathf.Abs(distance) < minSnappingDistance)
                    {
                        selectedTran.position = new Vector3(selectedTran.position.x, selectedTran.position.y + distance, selectedTran.position.z);
                        Debug.DrawLine(selectedTran.position + new Vector3(0, selectedCollider.bounds.extents.y, 0), colliderPos + new Vector3(0, collider.bounds.extents.y, 0), Color.red, 1);
                        return;
                    }

                    distance = compareYMin - selectedYMax;
                    if (Mathf.Abs(distance) < minSnappingDistance)
                    {
                        selectedTran.position = new Vector3(selectedTran.position.x, selectedTran.position.y + distance, selectedTran.position.z);
                        Debug.DrawLine(selectedTran.position + new Vector3(0, selectedCollider.bounds.extents.y, 0), colliderPos - new Vector3(0, collider.bounds.extents.y, 0), Color.red, 1);
                        return;
                    }

                }

                colliders = Physics.OverlapSphere(selectedTran.position - new Vector3(0, selectedBoundExtentY, 0), sphereRadius);
                foreach (Collider collider in colliders)
                {
                    if (collider.gameObject == selectedGO)
                    {
                        continue;
                    }

                    Vector3 colliderPos = collider.gameObject.transform.position;
                    

                    float compareYMax = collider.bounds.max.y;
                    float compareYMin = collider.bounds.min.y;


                    float distance = compareYMin - selectedYMin;
                    if (Mathf.Abs(distance) < minSnappingDistance)
                    {
                        selectedTran.position = new Vector3(selectedTran.position.x, selectedTran.position.y + distance, selectedTran.position.z);
                        Debug.DrawLine(selectedTran.position - new Vector3(0, selectedCollider.bounds.extents.y, 0), colliderPos - new Vector3(0, collider.bounds.extents.y, 0), Color.red, 1);
                        return;
                    }

                    distance = compareYMax - selectedYMin;
                    if (Mathf.Abs(distance) < minSnappingDistance)
                    {
                        selectedTran.position = new Vector3(selectedTran.position.x, selectedTran.position.y + distance, selectedTran.position.z);
                        Debug.DrawLine(selectedTran.position - new Vector3(0, selectedCollider.bounds.extents.y, 0), colliderPos + new Vector3(0, collider.bounds.extents.y, 0), Color.red, 1);
                        return;
                    }

                }
            }
            else if (axisMoving == axisMove.z)
            {
                float selectedBoundExtentZ = selectedCollider.bounds.extents.z;
                float selectedZMax = selectedCollider.bounds.max.z;
                float selecteZMin = selectedCollider.bounds.min.z;

                colliders = Physics.OverlapSphere(selectedTran.position + new Vector3(0, 0, selectedBoundExtentZ), sphereRadius);
                foreach (Collider collider in colliders)
                {
                    if (collider.gameObject == selectedGO)
                    {
                        continue;
                    }

                    Vector3 colliderPos = collider.gameObject.transform.position;
                    


                    float compareZMax = collider.bounds.max.z;
                    float compareZMin = collider.bounds.min.z;

                    float distance = compareZMax - selectedZMax;
                    if (Mathf.Abs(distance) < minSnappingDistance)
                    {
                        selectedTran.position = new Vector3(selectedTran.position.x, selectedTran.position.y, selectedTran.position.z + distance);
                        Debug.DrawLine(selectedTran.position + new Vector3(0, 0, selectedCollider.bounds.extents.z), colliderPos + new Vector3(0, 0, collider.bounds.extents.z), Color.red, 1);
                        return;
                    }

                    distance = compareZMin - selectedZMax;
                    if (Mathf.Abs(distance) < minSnappingDistance)
                    {
                        selectedTran.position = new Vector3(selectedTran.position.x, selectedTran.position.y, selectedTran.position.z + distance);
                        Debug.DrawLine(selectedTran.position + new Vector3(0, 0, selectedCollider.bounds.extents.z), colliderPos - new Vector3(0, 0, collider.bounds.extents.z), Color.red, 1);
                        return;
                    }

                }

                colliders = Physics.OverlapSphere(selectedTran.position - new Vector3(0, 0, selectedBoundExtentZ), sphereRadius);
                foreach (Collider collider in colliders)
                {
                    if (collider.gameObject == selectedGO)
                    {
                        continue;
                    }

                    Vector3 colliderPos = collider.gameObject.transform.position;
                    

                    float compareZMax = collider.bounds.max.z;
                    float compareZMin = collider.bounds.min.z;


                    float distance = compareZMin - selecteZMin;
                    if (Mathf.Abs(distance) < minSnappingDistance)
                    {
                        selectedTran.position = new Vector3(selectedTran.position.x, selectedTran.position.y, selectedTran.position.z + distance);
                        Debug.DrawLine(selectedTran.position - new Vector3(0, 0, selectedCollider.bounds.extents.z), colliderPos - new Vector3(0, 0, collider.bounds.extents.z), Color.red, 1);
                        return;
                    }

                    distance = compareZMax - selecteZMin;
                    if (Mathf.Abs(distance) < minSnappingDistance)
                    {
                        selectedTran.position = new Vector3(selectedTran.position.x, selectedTran.position.y, selectedTran.position.z + distance);
                        Debug.DrawLine(selectedTran.position - new Vector3(0, 0, selectedCollider.bounds.extents.z), colliderPos + new Vector3(0, 0, collider.bounds.extents.z), Color.red, 1);
                        return;
                    }

                }
            }

            

            RaycastHit firstHit;
            RaycastHit secondHit;

            if (axisMoving == axisMove.x)
            {
                if (Physics.Raycast(selectedTran.position, Vector3.right, out firstHit))
                {
                    
                    if (Physics.Raycast(firstHit.collider.transform.position, Vector3.right, out secondHit))
                    {
                        Vector3 snapPosition = MathF.Abs(secondHit.collider.gameObject.transform.position.x - firstHit.collider.gameObject.transform.position.x) * 
                            Vector3.left + firstHit.collider.gameObject.transform.position;

                        float distance = Mathf.Abs(snapPosition.x - selectedTran.position.x);

                        if (distance < minSnappingDistance)
                        {
                            selectedTran.position = new Vector3(snapPosition.x, selectedTran.position.y, selectedTran.position.z);
                            Debug.DrawLine(selectedTran.position, firstHit.collider.transform.position, Color.blue, 1);
                            Debug.DrawLine(firstHit.collider.transform.position, secondHit.collider.transform.position, Color.blue, 1);
                        }
                    }
                }
                if (Physics.Raycast(selectedTran.position, Vector3.left, out firstHit))
                {
                    
                    if (Physics.Raycast(firstHit.collider.transform.position, Vector3.left, out secondHit))
                    {
                        
                        Vector3 snapPosition = MathF.Abs(secondHit.collider.gameObject.transform.position.x - firstHit.collider.gameObject.transform.position.x) * 
                            Vector3.right + firstHit.collider.gameObject.transform.position;

                        float distance = Mathf.Abs(snapPosition.x - selectedTran.position.x);

                        if (distance < minSnappingDistance)
                        {
                            selectedTran.position = new Vector3(snapPosition.x, selectedTran.position.y, selectedTran.position.z);
                            Debug.DrawLine(selectedTran.position, firstHit.collider.transform.position, Color.blue, 1);
                            Debug.DrawLine(firstHit.collider.transform.position, secondHit.collider.transform.position, Color.blue, 1);
                        }
                    }
                }

                
            }
            else if (axisMoving == axisMove.y)
            {
                if (Physics.Raycast(selectedTran.position, Vector3.up, out firstHit))
                {

                    if (Physics.Raycast(firstHit.collider.transform.position, Vector3.up, out secondHit))
                    {
                        Vector3 snapPosition = MathF.Abs(secondHit.collider.gameObject.transform.position.y - firstHit.collider.gameObject.transform.position.y) * 
                            Vector3.down + firstHit.collider.gameObject.transform.position;

                        float distance = Mathf.Abs(snapPosition.y - selectedTran.position.y);

                        if (distance < minSnappingDistance)
                        {
                            selectedTran.position = new Vector3(selectedTran.position.x,snapPosition.y, selectedTran.position.z);
                            Debug.DrawLine(selectedTran.position, firstHit.collider.transform.position, Color.blue, 1);
                            Debug.DrawLine(firstHit.collider.transform.position, secondHit.collider.transform.position, Color.blue, 1);
                        }
                    }
                }
                if (Physics.Raycast(selectedTran.position, Vector3.down, out firstHit))
                {

                    if (Physics.Raycast(firstHit.collider.transform.position, Vector3.down, out secondHit))
                    {

                        Vector3 snapPosition = MathF.Abs(secondHit.collider.gameObject.transform.position.y - firstHit.collider.gameObject.transform.position.y) * 
                            Vector3.up + firstHit.collider.gameObject.transform.position;

                        float distance = Mathf.Abs(snapPosition.y - selectedTran.position.y);

                        if (distance < minSnappingDistance)
                        {
                            selectedTran.position = new Vector3(selectedTran.position.x, snapPosition.y, selectedTran.position.z);
                            Debug.DrawLine(selectedTran.position, firstHit.collider.transform.position, Color.blue, 1);
                            Debug.DrawLine(firstHit.collider.transform.position, secondHit.collider.transform.position, Color.blue, 1);
                        }
                    }
                }

            }
            else
            {
                if (Physics.Raycast(selectedTran.position, Vector3.forward, out firstHit))
                {

                    if (Physics.Raycast(firstHit.collider.transform.position, Vector3.forward, out secondHit))
                    {
                        Vector3 snapPosition = MathF.Abs(secondHit.collider.gameObject.transform.position.y - firstHit.collider.gameObject.transform.position.y) * 
                            Vector3.back + firstHit.collider.gameObject.transform.position;

                        float distance = Mathf.Abs(snapPosition.y - selectedTran.position.y);

                        if (distance < minSnappingDistance)
                        {
                            selectedTran.position = new Vector3(selectedTran.position.x, selectedTran.position.y, snapPosition.z);
                            Debug.DrawLine(selectedTran.position, firstHit.collider.transform.position, Color.blue, 1);
                            Debug.DrawLine(firstHit.collider.transform.position, secondHit.collider.transform.position, Color.blue, 1);
                        }
                    }
                }
                if (Physics.Raycast(selectedTran.position, Vector3.back, out firstHit))
                {

                    if (Physics.Raycast(firstHit.collider.transform.position, Vector3.back, out secondHit))
                    {

                        Vector3 snapPosition = MathF.Abs(secondHit.collider.gameObject.transform.position.y - firstHit.collider.gameObject.transform.position.y) * 
                            Vector3.forward + firstHit.collider.gameObject.transform.position;

                        float distance = Mathf.Abs(snapPosition.y - selectedTran.position.y);

                        if (distance < minSnappingDistance)
                        {
                            selectedTran.position = new Vector3(selectedTran.position.x, selectedTran.position.y, snapPosition.z);
                            Debug.DrawLine(selectedTran.position, firstHit.collider.transform.position, Color.blue, 1);
                            Debug.DrawLine(firstHit.collider.transform.position, secondHit.collider.transform.position, Color.blue, 1);
                        }
                    }
                }
            }

            pastPosition = selectedTran.position;

        }

        else if(currTool == Tool.Rotate)
        {
            GameObject selectedGO = Selection.gameObjects[0];
            Transform selectedTran = selectedGO.transform;


            Undo.RecordObject(selectedGO.transform, UNDO_STR_SNAP);


            if ((initialGO == null) || (initialGO && initialGO != selectedGO))
            {
                initialGO = selectedGO;
                pastRotation = selectedGO.transform.rotation;
                selectedCollider = selectedGO.GetComponent<Collider>();
                return;
            }




            if (pastRotation.x != selectedGO.transform.rotation.x)
                axisMoving = axisMove.x;

            else if (pastRotation.y != selectedGO.transform.rotation.y)
                axisMoving = axisMove.y;

            else
                axisMoving = axisMove.z;






            Collider[] colliders = Physics.OverlapSphere(selectedTran.position, sphereRadius);

            foreach (Collider collider in colliders)
            {
                if (collider.gameObject == selectedGO)
                {
                    continue;
                }

                Quaternion colliderRot = collider.gameObject.transform.rotation;
                if (axisMoving == axisMove.x)
                {
                    float distance = Quaternion.Angle(selectedGO.transform.rotation, colliderRot);
                    if (distance < minSnappingAngle)
                    {
                        selectedTran.rotation = colliderRot;
                        Debug.Log("snap rotate");
                        return;
                    }


                }
                else if (axisMoving == axisMove.y)
                {
                    float distance = Quaternion.Angle(selectedGO.transform.rotation, colliderRot);
                    if (distance < minSnappingAngle)
                    {
                        selectedTran.rotation = colliderRot;
                        Debug.Log("snap rotate");
                        return;
                    }
                }
                else
                {
                    float distance = Quaternion.Angle(selectedGO.transform.rotation, colliderRot);
                    if (distance < minSnappingAngle)
                    {
                        selectedTran.rotation = colliderRot;
                        Debug.Log("snap rotate");
                        return;
                    }
                }


            }
        }

        else
        {
            initialGO = null;
        }


    }

    public static Vector3 Round(this Vector3 v)
    {
        v.x = (float)Math.Round( v.x,degree);
        v.y = (float)Math.Round(v.y,degree);
        v.z = (float)Math.Round(v.z,degree);
        return v;
    }


    
}
